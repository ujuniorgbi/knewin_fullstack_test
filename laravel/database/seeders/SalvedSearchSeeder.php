<?php

namespace Database\Seeders;

use App\Models\SalvedSearch;
use DB;
use Illuminate\Database\Seeder;

class SalvedSearchSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('salved_searches')->insert([
            'term' => 'AMBEV',
            'user_id' => '1',
        ]);
        DB::table('salved_searches')->insert([
            'term' => 'PETROBRAS',
            'user_id' => '1',
        ]);
        DB::table('salved_searches')->insert([
            'term' => 'PEPSI',
            'user_id' => '1',
        ]);
        DB::table('salved_searches')->insert([
            'term' => 'GOOGLE',
            'user_id' => '1',
        ]);

    }
}
