<?php

namespace App\Providers;

use Illuminate\Support\Facades\Response;
use Illuminate\Support\ServiceProvider;
use Throwable;

class ResponseServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //

        $vm = $this;
 
        Response::macro('success', function ($info, $status = 200, $headers = []) use ($vm) {
            return $vm->success($info, $status, $headers);
        });

        Response::macro('successCreated', function ($info, $headers = []) use ($vm) {
            return $vm->successCreated($info, $headers);
        });

        Response::macro('fail', function ($info, $status = 400, $headers = []) use ($vm) {
            return $vm->fail($info, $status, $headers);
        });

        Response::macro('validationFail', function ($info, $headers = []) use ($vm) {
            return $vm->validationFail($info, $headers);
        });

        Response::macro('unauthenticated', function ($info, $headers = []) use ($vm) {
            return $vm->unauthenticated($info, $headers);
        });

        Response::macro('forbidden', function ($info, $headers = []) use ($vm) {
            return $vm->forbidden($info, $headers);
        });

        Response::macro('notFound', function ($info, $headers = []) use ($vm) {
            return $vm->notFound($info, $headers);
        });

        Response::macro('notAcceptable', function ($info, $headers = []) use ($vm) {
            return $vm->notAcceptable($info, $headers);
        });

        Response::macro('error', function ($info, $status = 500, $headers = []) use ($vm) {
            return $vm->error($info, $status, $headers);
        });
    }

    public function success($info, $status = 200, $headers = [])
    {
        $data = $info['data'] ?? [];
        $links = $info['links'] ?? [];

        $response = array_merge($this->getDefaultResponseScheme('success', $links), [
            'data' => $data,
        ]);

        return response()->json($response, $status, $headers, JSON_UNESCAPED_SLASHES);
    }

    public function fail($info, $status = 400, $headers = [])
    {
        return $this->makeErrorOrFail('fail', $info, $status, $headers);
    }

    public function error($info, $status = 500, $headers = [])
    {
        return $this->makeErrorOrFail('error', $info, $status, $headers);
    }

    public function successCreated($info, $headers = [])
    {
        return $this->success($info, 201, $headers);
    }

    public function validationFail($info, $headers = [])
    {
        $info['message']??='Some validation failure has occurred';
        $info['type']??='VALIDATION';
        return $this->fail($info, 422, $headers);
    }

    public function unauthenticated($info, $headers = [])
    {
        $info['message']??='You are not authenticated to access this resource';
        $info['type']??='UNAUTHENTICATED';
        return $this->fail($info, 401, $headers);
    }

    public function forbidden($info, $headers = [])
    {
        $info['message']??='You are not authorized to access this resource';
        $info['type']??='FORBIDDEN';
        return $this->fail($info, 403, $headers);
    }

    public function notFound($info, $headers = [])
    {
        $info['message']??='This resource was not found';
        $info['type']??='NOT_FOUND';
        return $this->fail($info, 404, $headers);
    }

    public function notAcceptable($info, $headers = [])
    {
        $info['message']??='This request is not acceptable';
        $info['type']??='NOT_ACCEPTABLE';
        return $this->fail($info, 406, $headers);
    }

    private function getDefaultResponseScheme($type, $links)
    {
        $response = [
            'status' => $type,
            'links' => [
                'self' => request()->fullUrl(),
            ],
        ];

        if (count($links)) {
            foreach ($links as $name => $url) {
                $response['links'][$name] = $url;
            }
        }

        return $response;
    }

    private function makeErrorOrFail($type = 'fail', $info = [], $status = 400, $headers = [])
    {
        $message = $info['message'] ?? 'Some error occurred';
        $details = $info['details'] ?? [];
        $links = $info['links'] ?? [];
        $exception = $info['error'] ?? null;
        $errorType = $info['type'] ?? 'GENERIC';

        if (empty($info['type']) && $status === 500) {
            $errorType = 'SERVER';
        }

        $failScheme = [
            'type' => $errorType,
            'message' => $message,
            'details' => $details,
        ];

        if ($exception) {
            if (config('app.debug')) {
                $failScheme['exception'] = $this->exceptionToArray($exception);
            }

            $failScheme['message'] = empty($info['message']) ? $exception->getMessage() : $message;
        }

        $response = array_merge($this->getDefaultResponseScheme($type, $links), $failScheme);

        return response()->json($response, $status, $headers, JSON_UNESCAPED_SLASHES);
    }

    private function exceptionToArray(Throwable $error)
    {
        return [
            'code' => $error->getCode(),
            'message' => $error->getMessage(),
            'file' => $error->getFile(),
            'line' => $error->getLine(),
            'stack' => $error->getTrace(),
            'previous' => $error->getPrevious(),
        ];
    }
}
