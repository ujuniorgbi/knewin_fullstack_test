<?php

namespace App\Models;

use App\Search\Searchable;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class News extends Model
{
    use HasFactory;
    use Searchable;
    
    protected $table = 'news';
    
    protected $fillable = [
        'content',
        'caption',
        'source',
        'title',
        'publication_date',
        'url',
    ];

    protected $casts = [
        'publication_date' => 'datetime',
    ];
}
