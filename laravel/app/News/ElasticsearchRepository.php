<?php

namespace App\News;

use App\Models\News as ModelsNews;
use App\News;
use Elasticsearch\Client;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Collection;

class ElasticsearchRepository implements NewsRepository
{
    /** @var \Elasticsearch\Client */
    private $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        $this->elasticsearch = $elasticsearch;
    }

    public function search(array $query): Collection
    {
        $items = $this->searchOnElasticsearch($query);
        return $this->buildCollection($items);
    }

    private function searchOnElasticsearch(array $query): array
    {
        $model = new ModelsNews();

        $items = $this->elasticsearch->search([
            'index' => $model->getSearchIndex(),
            'type' => $model->getSearchType(),
            'body' => [
                'query' => [
                    'bool' => [
                        'must' => [
                            ['range' => [
                                'publication_date' => ['gt' => $query['start_date'],'lt' => $query['end_date']]
                            ]],
                            ['match' => [
                                'source' => $query['source']]],
                            ],
                            'should'=>[
                                ['query_string' => [
                                    'default_field' => '_all',
                                    'query' => $query['search']]
                                ]
                            ]

                    ]
                ],

            ],
        ]);

        return $items;
    }

    private function buildCollection(array $items): Collection
    {
        $ids = Arr::pluck($items['hits']['hits'], '_id');

        return ModelsNews::findMany($ids, ['id', 'title', 'caption','source', 'publication_date'])
            ->sortBy(function ($article) use ($ids) {
                return array_search($article->getKey(), $ids);
            });
    }
}