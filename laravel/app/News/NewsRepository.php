<?php

namespace App\News;

use App\News;
use Illuminate\Database\Eloquent\Collection;

interface NewsRepository
{
    public function search(array $query): Collection;
}