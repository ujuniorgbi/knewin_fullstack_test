<?php

namespace App\Http\Controllers;

use App\Models\SalvedSearch;
use App\Models\User;
use Illuminate\Http\Request;

class SalvedSearchController extends Controller
{
    public function index(){
        $user = auth('api')->user();
        return response()->json(['terms' => $user->salvedSearchs()->get()]);
    }
}
