<?php

namespace App\Http\Controllers;

use App\Http\Requests\jsonRequest;
use App\Http\Requests\newsIndexRequest;
use Illuminate\Http\Request;
use App\Models\News;
use App\News\NewsRepository;
use Artisan;
use DB;

class NewsController extends Controller
{

    public function index(NewsRepository $repository, newsIndexRequest $request)
    {
        try {
            $data =  $request->only('search', 'source', 'start_date', 'end_date');
            return ['news' => $repository->search((array) $data)];
        } catch (\Throwable $th) {
            return response()->json(['msg' => 'Ocorreu um erro inesperado'], 500);
        }
    }

    public function show(News $news)
    {
        try {
            return response()->json(['news' =>  $news]);
        } catch (\Throwable $th) {
            return response()->json(['msg' => 'Ocorreu um erro inesperado'], 500);
        }
    }

    public function store(jsonRequest $request)
    {
        try {
            $file = $request->file('news');
            $json = file_get_contents($file);
            $news = json_decode($json);

            DB::beginTransaction();

            foreach ($news as $new) {
                $NewsData = array(
                    'title' => $new->titulo,
                    'content' => $new->conteudo,
                    'caption' => $new->subtitulo ?? null,
                    'source' => $new->fonte == null || $new->fonte ==  "" ? null : $new->fonte,
                    'url' => $new->url ?? null,
                    'publication_date' => $new->data_publicacao
                );
                News::create($NewsData);
            }

            DB::commit();

            Artisan::call('search:reindex');

            return response()->json(['msg' => 'Os arquivos foram indexados com sucesso!']);
        } catch (\Throwable $th) {
            return response()->json(['msg' => 'Ocorreu um erro inesperado'], 500);
        }
    }

    public function sources()
    {
        try {
            $sources = News::whereNotNull('source')->groupBy('source')->select('source')->get();
            return response()->json(['sources' => $sources]);
        } catch (\Throwable $th) {
            return response()->json(['msg' => 'Ocorreu um erro inesperado'], 500);
        }
    }
}
