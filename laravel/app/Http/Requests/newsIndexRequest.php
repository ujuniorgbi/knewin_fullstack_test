<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class newsIndexRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'search' => 'required',
            'source' => 'required',
            'start_date' => 'required|date',
            'end_date' => 'required|date',

        ];
    }

    public function messages()
    {
        return [
            'search.required' => 'String de busca é necessaria',
            'source.required' => 'A fonte é necessaria',
            'start_date.required' => 'Data de inicio é necessaria',
            'start_date.date' => 'Data de inicio deve ser uma data valida',
            'end_date.required' => 'Data de final é necessaria',
            'end_date.date' => 'Data de final deve ser uma data valida',

        ];
    }
}
