<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class jsonRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
                'news' => 'required|mimes:json',        
        ];

    }

    public function messages()
    {
        return [
            'news.required' => 'O arquivo de noticias é necessario',
            'news.mimes' => 'Deve ser um arquivo json',

        ];
    }

}
