<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\ResponseFactory;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;


class ApiProtectedRoute extends BaseMiddleware
{
    protected $responseFactory;

    public function __construct(ResponseFactory $responseFactory)
    {
        $this->responseFactory = $responseFactory;
    }
    /**
    * Handle an incoming request.
    *
   * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
   public function handle(Request $request, Closure $next)
   {

    try {
        $user = JWTAuth::parseToken()->authenticate();
    } catch (\Exception $e) {
        if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
            return response()->json(['status' => 'Token is Invalid'],401);
        }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
            return response()->json(['status' => 'Token is Expired'],401);
        }else{
            return response()->json(['status' => 'Authorization Token not found'],401);
        }
    }

    $request->headers->set('Accept', 'application/json');

    $response = $next($request);

    if(!$response instanceof JsonResponse)
    {
        $response = $this->responseFactory->json(
            $response->content(),
            $response->status(),
            $response->headers->all()
        );
    }

    return $response;

   }
}
