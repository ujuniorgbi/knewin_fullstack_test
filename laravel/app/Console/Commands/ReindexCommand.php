<?php

namespace App\Console\Commands;

use App\Models\News as ModelsNews;
use App\News;
use Elasticsearch\Client;
use Illuminate\Console\Command;

class ReindexCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'search:reindex';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Indexa todas as noticias no Elasticsearch';

    /** @var \Elasticsearch\Client */
    private $elasticsearch;

    public function __construct(Client $elasticsearch)
    {
        parent::__construct();

        $this->elasticsearch = $elasticsearch;
    }

    public function handle()
    {
        $this->info('Indexando as Noticias. isso pode demorar um pouco...');

        foreach (ModelsNews::cursor() as $new)
        {
            $this->elasticsearch->index([
                'index' => $new->getSearchIndex(),
                'type' => $new->getSearchType(),
                'id' => $new->getKey(),
                'body' => $new->toSearchArray(),
            ]);

            $this->output->write('.');
        }

        $this->info("\nPronto!");
    }
}