<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\NewsController;
use App\Http\Controllers\SalvedSearchController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::group(['prefix' => 'auth'], function () {
    Route::post('login', [AuthController::class, 'login']);
    Route::get('me', [AuthController::class, 'me'])->middleware('apiJWT'); 
    Route::get('logout', [AuthController::class, 'logout'])->middleware('apiJWT'); 
    Route::get('refresh', [AuthController::class, 'refresh'])->middleware('apiJWT'); 
});

Route::middleware(['apiJWT'])->group(function () {
    Route::apiResource('news', NewsController::class);
    Route::get('searchTerms', [SalvedSearchController::class, 'index']);
    Route::get('sources', [NewsController::class, 'sources']);
});