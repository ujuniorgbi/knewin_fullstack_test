
# Knewin_fullstack_test

  
#### Como instalar

- Na pasta do docker copiar '.env.example' e renomear como '.env'

- Na pasta do docker executar o comando `docker-compose up -d --build`

- entrar no [pgadmin](localhost:5050)

- criar o server 'laravel' com o host 'postgres 'com a senha 'secret', database 'laravel' e o  schema 'laravel' 

- Na pasta do laravel copiar .env.example e renomear como como '.env' 

- rodar comando `composer install` no container do apache
  
- rodar comando `php artisan jwt:secret` no container do apache

- rodar comando `php artisan migrate --seed` no container do apache

 #### Como utilizar
 Há um erro no arquivo `noticias.json`, uma **'}'** extra na linha 798, a solução de menor esforço e custo é editar o json removendo a chave extra.
 (o arquivo não vai passar pela validação do laravel com a chave extra, pois não é considerado um arquivo json )

- Para facilitar o uso foi criado um seeder com os termos do arquivo `pesquisas_salvas.json`
- Somente a categoria de 'Noticias' no modal de fontes vai retornar resultados
- As fontes serão extraidas do arquivo `noticias.json`,(todas as noticias tem a mesma fonte 'The Guardian'), para facilitar o teste,
na raiz do repositorio tem um arquivo `noticias_corrigidas.json` que foi corrigido o problema da **'}'** extra, bem como alterado a fonte de algumas noticias
para aumentar a variedade. 

 A indexação será automática no momento que for enviado o Arquivo `noticias.json` ou  `noticias_corrigidas.json`
 - link para acessar: [Quasar](localhost:8081)
 - O usuario e a senha já foram definidos na inicialização das variaveis 
 - Fazer upload do arquivo no canto superior direito da tela
 - realizar busca como desejado
  
#### Sobre

- Foi utilizada o frameword Quasar devido a minha familiaridade com os componentes e minha experiência previa  
- Em minhas aplicações laravel sempre utilizei o Sanctum para autenticação, diante disso e do prazo, foi realizada somente uma implementação basica do JWT
- O componente de dialogo do quasar sera absoluto e centralizado horizontalmente independente do contexto que for chamado, por isso o modal não foi replicado de maneira 100% fiel, pois seria necessaria a implementação de um componente personalizado 

#### Solução de erros comuns

- caso não consiga entrar no pgadmin executar `sudo chown -R 5050:5050 /pasta/do/volume/pgadmin`

- Se não conseguir pelo criar o banco pelo pgadmin cria com `psql -h localhost -d postgres -U postgres`

- Caso no Laravel ocorra o erro 'Permission denied' só executar o comando `chmod -R 777 storage` no container do apache

- caso o rewrite module não estiver ativado executar o comando `a2enmod rewrite` e reiniciar o apache

- caso o ElasticSearch dê erro de memoria rodar comando `wsl -d docker-desktop` e `sysctl -w vm.max_map_count=262144` *no Windows

- .env do laravel deve conter:
  - DB_CONNECTION=pgsql
  - DB_HOST=postgres
  - DB_PORT=5432
  - DB_DATABASE=laravel
  - DB_USERNAME=postgres
  - DB_PASSWORD=secret
  - ELASTICSEARCH_HOSTS="elastic:9200"
