export default {
  data: () => ({}),

  methods: {
    getDateTime(timestamp) {
        var date = new Date(timestamp);
        return date.toLocaleString('pt-BR')
    },
  },
};
