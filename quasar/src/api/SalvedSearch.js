import { makeRequest } from "./http";

export default {
    terms: () => makeRequest('get', `/searchTerms`),
}
