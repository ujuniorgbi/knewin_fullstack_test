import { makeRequest } from "./http";
import { Api } from "./http";

export default {
    index: params => makeRequest('get', `/news`,null, {params} ),
    show: id => Api.show('news', id),
    sources: () => makeRequest('get', `/sources`),

    store(file) {
        const form = new FormData()
        form.append('news', file)
        return makeRequest('post', `/news`, form, { 
            headers: {
                'Content-Type': 'multipart/form-data'
            }
        })
    }
}
