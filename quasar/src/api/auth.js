import { makeRequest } from "./http";

export default {
  login: (data) => makeRequest("post", "/auth/login", data),
  logout: () => makeRequest("get", "/auth/logout"),
  me: () => makeRequest("get", "/auth/me"),
};
