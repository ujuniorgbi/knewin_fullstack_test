import { LocalStorage } from 'quasar'

export default ({ router }) => {
    
    router.beforeEach((to, from, next) => {

        LocalStorage.set('previousRoute', from.path)

        let loggedIn = LocalStorage.getItem('loggedIn')

        if(loggedIn && to.path == '/autenticar'){
            next('/')
            return
        }

        if(!loggedIn && to.path !== '/autenticar'){
            next('/autenticar')
            return
        }
        else {
            next()
        }

        

    })

}
